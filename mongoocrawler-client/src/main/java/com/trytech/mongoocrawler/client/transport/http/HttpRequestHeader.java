package com.trytech.mongoocrawler.client.transport.http;

/**
 * Created by coliza on 2017/7/31.
 */
public class HttpRequestHeader extends HttpHeader {
    public final static String ACCEPT = "Accept";
    public final static String ACCEPT_CHARSET = "Accept-Charset";
    public final static String ACCEPT_ENCODING = "Accept-Encoding";
    public final static String ACCEPT_LANGUAGE = "Accept-Language";
    public final static String ACCEPT_RANGES = "Accept-Ranges";
    public final static String AUTHORIZATION = "Authorization";
    public final static String CACHE_CONTROL = "Cache-Control";
    public final static String CONNECTION = "Connection";
    public final static String COOKIE = "Cookie";
    public final static String CONTENT_LENGTH = "Content-Length";
    public final static String CONTENT_TYPE = "Content-Type";
    public final static String DATE = "Date";
    public final static String EXPECT = "Expect";
    public final static String FROM = "From";
    public final static String HOST = "Host";
    public final static String IF_MATCH = "If-Match";
    public final static String IF_MODIFIED_SINCE = "If-Modified-Since";
    public final static String IF_NONE_MATCH = "If-None-Match";
    public final static String IF_RANGE = "If-Range";
    public final static String IF_UNMODIFIED_SINCE = "If-Unmodified-Since";
    public final static String MAX_FORWARDS = "Max-Forwards";
    public final static String PRAGMA = "Pragma";
    public final static String PROXY_AUTHORIZATION = "Proxy-Authorization";
    public final static String RANGE = "Range";
    public final static String REFER = "Referer";
    public final static String TE = "TE";
    public final static String UPGRADE = "Upgrade";
    public final static String USER_AGENT = "User-Agent";
    public final static String VIA = "Via";
    public final static String WARNING = "Warning";

    private String accept;
    private String acceptCharset;
    private String acceptEncoding;
    private String acceptLanguage;
    private String acceptRange;
    private String authorization;
    private String cacheControl;
    private String connection;
    private String cookie;
    private String contentLength;
    private String contentType;
    private String date;
    private String expect;
    private String from;
    private String host;
    private String ifMatch;
    private String ifModifiedSince;
    private String ifNoneMatch;
    private String ifRange;
    private String ifUnmodifiedSince;
    private String maxForwards;
    private String pragma;
    private String proxyAuthorization;
    private String range;
    private String te;
    private String upgrade;
    private String userAgent;
    private String via;
    private String warning;

    public String getAccept() {
        return accept;
    }

    public void setAccept(String accept) {
        this.accept = accept;
    }

    public String getAcceptCharset() {
        return acceptCharset;
    }

    public void setAcceptCharset(String acceptCharset) {
        this.acceptCharset = acceptCharset;
    }

    public String getAcceptEncoding() {
        return acceptEncoding;
    }

    public void setAcceptEncoding(String acceptEncoding) {
        this.acceptEncoding = acceptEncoding;
    }

    public String getAcceptLanguage() {
        return acceptLanguage;
    }

    public void setAcceptLanguage(String acceptLanguage) {
        this.acceptLanguage = acceptLanguage;
    }

    public String getAcceptRange() {
        return acceptRange;
    }

    public void setAcceptRange(String acceptRange) {
        this.acceptRange = acceptRange;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getCacheControl() {
        return cacheControl;
    }

    public void setCacheControl(String cacheControl) {
        this.cacheControl = cacheControl;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public String getContentLength() {
        return contentLength;
    }

    public void setContentLength(String contentLength) {
        this.contentLength = contentLength;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getExpect() {
        return expect;
    }

    public void setExpect(String expect) {
        this.expect = expect;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getIfMatch() {
        return ifMatch;
    }

    public void setIfMatch(String ifMatch) {
        this.ifMatch = ifMatch;
    }

    public String getIfModifiedSince() {
        return ifModifiedSince;
    }

    public void setIfModifiedSince(String ifModifiedSince) {
        this.ifModifiedSince = ifModifiedSince;
    }

    public String getIfNoneMatch() {
        return ifNoneMatch;
    }

    public void setIfNoneMatch(String ifNoneMatch) {
        this.ifNoneMatch = ifNoneMatch;
    }

    public String getIfRange() {
        return ifRange;
    }

    public void setIfRange(String ifRange) {
        this.ifRange = ifRange;
    }

    public String getIfUnmodifiedSince() {
        return ifUnmodifiedSince;
    }

    public void setIfUnmodifiedSince(String ifUnmodifiedSince) {
        this.ifUnmodifiedSince = ifUnmodifiedSince;
    }

    public String getMaxForwards() {
        return maxForwards;
    }

    public void setMaxForwards(String maxForwards) {
        this.maxForwards = maxForwards;
    }

    public String getPragma() {
        return pragma;
    }

    public void setPragma(String pragma) {
        this.pragma = pragma;
    }

    public String getProxyAuthorization() {
        return proxyAuthorization;
    }

    public void setProxyAuthorization(String proxyAuthorization) {
        this.proxyAuthorization = proxyAuthorization;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getTe() {
        return te;
    }

    public void setTe(String te) {
        this.te = te;
    }

    public String getUpgrade() {
        return upgrade;
    }

    public void setUpgrade(String upgrade) {
        this.upgrade = upgrade;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getWarning() {
        return warning;
    }

    public void setWarning(String warning) {
        this.warning = warning;
    }

    public static class Builder{
        public static Builder newBuilder(){
            return new Builder();
        }
    }
}
