package com.trytech.mongoocrawler.common.bean;

/**
 * Created by coliza on 2018/5/24.
 */
public class ServerConfig {
    private int mode;
    private String modeLabel;
    private int crawlerCount;
    private int runStatus;
    private String runStatusLabel;

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public String getModeLabel() {
        return modeLabel;
    }

    public void setModeLabel(String modeLabel) {
        this.modeLabel = modeLabel;
    }

    public int getCrawlerCount() {
        return crawlerCount;
    }

    public void setCrawlerCount(int crawlerCount) {
        this.crawlerCount = crawlerCount;
    }

    public int getRunStatus() {
        return runStatus;
    }

    public void setRunStatus(int runStatus) {
        this.runStatus = runStatus;
    }

    public String getRunStatusLabel() {
        return runStatusLabel;
    }

    public void setRunStatusLabel(String runStatusLabel) {
        this.runStatusLabel = runStatusLabel;
    }

    public enum Status {

    }
}
