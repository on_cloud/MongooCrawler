package com.trytech.mongoocrawler.common.enums;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年09月03日
 */
public enum CrawlerStatus {
    STOPPED(0, "停止"), RUNNING(1, "运行中");
    private int code;
    private String value;

    CrawlerStatus(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public static String getStatusLabel(int code) {
        switch (code) {
            case 1:
                return RUNNING.getValue();
            default:
                return STOPPED.getValue();
        }
    }

    public static CrawlerStatus from(int status) {
        switch (status) {
            case 0:
                return STOPPED;
            case 1:
                return RUNNING;
            default:
                return null;
        }

    }

    public int getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public boolean equalsTo(int status) {
        return code == status;
    }

    public boolean equalsTo(CrawlerStatus status) {
        return code == status.getCode();
    }
}