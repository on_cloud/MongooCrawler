package com.trytech.mongoocrawler.common.transport.event;

import com.trytech.mongoocrawler.common.enums.CrawlerStatus;

/**
 * Created by coliza on 2018/9/16.
 */
public class CrawlerStateChangedEvent implements IEvent {
    private IEventSource source;
    private CrawlerStatus crawlerStatus;

    public CrawlerStateChangedEvent(IEventSource source) {
        this.source = source;
    }

    @Override
    public IEventSource getSource() {
        return source;
    }

    @Override
    public void setSource(IEventSource source) {
        this.source = source;
    }

    public CrawlerStatus getCrawlerStatus() {
        return crawlerStatus;
    }

    public void setCrawlerStatus(CrawlerStatus crawlerStatus) {
        this.crawlerStatus = crawlerStatus;
    }
}
