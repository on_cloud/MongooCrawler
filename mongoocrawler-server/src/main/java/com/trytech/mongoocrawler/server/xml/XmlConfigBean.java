package com.trytech.mongoocrawler.server.xml;

import com.trytech.mongoocrawler.server.common.db.CrawlerDataSource;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * XML bean对象
 */
public class XmlConfigBean {
    private static Map<String, CrawlerDataSource> dataSourceMap = new HashMap<String, CrawlerDataSource>();
    private ConcurrentHashMap<String, CrawlerXmlConfigBean> crawlers = new ConcurrentHashMap<String, CrawlerXmlConfigBean>();
    /***
     * 模式：0-本地单机模式 1-分布式模式
     */
    private ModeConfigBean modeConfigBean;
    private CacheXmlConfigBean cacheXmlConfigBean;
    private MonitorConfigBean monitorConfigBean;

    public static CrawlerDataSource getDataSource(String name) {
        return dataSourceMap.get(name);
    }

    public static Collection<CrawlerDataSource> getAllDataSources() {
        return dataSourceMap.values();
    }

    public ConcurrentHashMap<String, CrawlerXmlConfigBean> getCrawlers() {
        return crawlers;
    }

    public void setCrawlers(ConcurrentHashMap<String, CrawlerXmlConfigBean> crawlers) {
        this.crawlers = crawlers;
    }

    public CacheXmlConfigBean getCacheXmlConfigBean() {
        return cacheXmlConfigBean;
    }

    public void setCacheXmlConfigBean(CacheXmlConfigBean cacheXmlConfigBean) {
        this.cacheXmlConfigBean = cacheXmlConfigBean;
    }

    public ModeConfigBean getModeConfigBean() {
        return modeConfigBean;
    }

    public void setModeConfigBean(ModeConfigBean modeConfigBean) {
        this.modeConfigBean = modeConfigBean;
    }

    public MonitorConfigBean getMonitorConfigBean() {
        return monitorConfigBean;
    }

    public void setMonitorConfigBean(MonitorConfigBean monitorConfigBean) {
        this.monitorConfigBean = monitorConfigBean;
    }

    public void setDataSource(String name, CrawlerDataSource dataSource) {
        dataSourceMap.put(name, dataSource);
    }

    public void registerCrawler(String name, CrawlerXmlConfigBean crawlerXmlConfigBean){
        if(StringUtils.isNotEmpty(name) && crawlerXmlConfigBean != null){
            crawlers.put(name, crawlerXmlConfigBean);
        }
    }

    public void registerCache(CacheXmlConfigBean cacheXmlConfigBean){
        if(cacheXmlConfigBean != null) {
            this.cacheXmlConfigBean = cacheXmlConfigBean;
        }
    }
}
