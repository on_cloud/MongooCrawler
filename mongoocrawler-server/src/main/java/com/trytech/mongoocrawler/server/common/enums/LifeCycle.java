package com.trytech.mongoocrawler.server.common.enums;

public enum LifeCycle {
    INITIALIZED(0), RUNNING(1), PAUSED(2), FREE(3), DESTORYED(4);
    private int status;

    LifeCycle(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public boolean isFree() {
        return FREE.getStatus() == status;
    }
}