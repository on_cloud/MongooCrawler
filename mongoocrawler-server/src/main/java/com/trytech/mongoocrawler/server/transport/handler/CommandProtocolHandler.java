package com.trytech.mongoocrawler.server.transport.handler;

import com.trytech.mongoocrawler.common.bean.MonitorData;
import com.trytech.mongoocrawler.common.enums.CrawlerStatus;
import com.trytech.mongoocrawler.common.transport.protocol.AbstractProtocol;
import com.trytech.mongoocrawler.common.transport.protocol.CommandProtocol;
import com.trytech.mongoocrawler.common.transport.protocol.MonitorProtocol;
import com.trytech.mongoocrawler.common.transport.protocol.ProtocolFilterChain;
import com.trytech.mongoocrawler.common.transport.protocol.UrlProtocol;
import com.trytech.mongoocrawler.server.CrawlerContext;
import com.trytech.mongoocrawler.server.CrawlerSession;
import com.trytech.mongoocrawler.server.DistributedCrawlerSession;
import com.trytech.mongoocrawler.server.common.enums.LifeCycle;
import com.trytech.mongoocrawler.server.parser.lianjia.LianjiaHtmlParser;
import com.trytech.mongoocrawler.server.transport.UrlParserPair;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年07月01日
 */
public class CommandProtocolHandler extends ServerProtocolHandler {

    public CommandProtocolHandler(CrawlerContext crawlerContext) {
        super(crawlerContext);
    }

    @Override
    public AbstractProtocol doHandle(ProtocolFilterChain filterChain, AbstractProtocol protocol) throws Exception {
        CommandProtocol.Command command = ((CommandProtocol) protocol).getCommand();
        if (command.equalsTo(CommandProtocol.Command.GET_URL)) {
            //获取session
            DistributedCrawlerSession crawlerSession = (DistributedCrawlerSession) crawlerContext.getSession(protocol
                    .getSessionId());

            UrlParserPair urlParserPair = crawlerSession.fetchUrl();
            //从url池中获取url
            UrlProtocol urlProtocol = new UrlProtocol();
            urlProtocol.setTraceId("001");
            urlProtocol.setUrl(urlParserPair.getUrl());
            return urlProtocol;
        } else if (command.equalsTo(CommandProtocol.Command.HEARTBEAT)) {
            //如果是心跳
            return null;
        } else if (command.equalsTo(CommandProtocol.Command.CRAWL)) {
            //从url池中获取url
            MonitorProtocol monitorProtocol = new MonitorProtocol();
            monitorProtocol.setTraceId(protocol.getTraceId());
            MonitorData monitorData = MonitorData.getInstance();
            monitorProtocol.setMonitorData(monitorData);
            //如果是爬取任务
            CrawlerSession crawlerSession = crawlerContext.getSession(protocol
                    .getSessionId());
            monitorProtocol.setSessionId(crawlerSession.getSessionId());
            LifeCycle lifeCycle = crawlerSession.getLifeCycle();
            monitorData.getServerConfig().setRunStatus(CrawlerStatus.RUNNING.getCode());
            monitorData.getServerConfig().setRunStatusLabel(CrawlerStatus.RUNNING.getValue());
            if (!lifeCycle.isFree()) {
                return monitorProtocol;
            }
            monitorData.getServerConfig().setRunStatus(CrawlerStatus.STOPPED.getCode());
            monitorData.getServerConfig().setRunStatusLabel(CrawlerStatus.STOPPED.getValue());
            String url = ((CommandProtocol) protocol).getData().toString();
            UrlParserPair urlParserPair = new UrlParserPair(url, new LianjiaHtmlParser());
            crawlerSession.pushUrl(urlParserPair);

            return monitorProtocol;
        }
        return filterChain.doFilter(protocol);
    }

    @Override
    protected boolean checkProtocol(AbstractProtocol abstractProtocol) {
        return abstractProtocol instanceof CommandProtocol;
    }
}