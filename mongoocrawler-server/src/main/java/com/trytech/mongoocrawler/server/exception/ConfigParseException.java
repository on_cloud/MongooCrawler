package com.trytech.mongoocrawler.server.exception;

/**
 * Created by coliza on 2018/5/1.
 */
public class ConfigParseException extends Exception {
    private String message;

    public ConfigParseException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String toString() {
        return this.message;
    }
}
